# Discourse Ahwaa

This plugin contains all the customizations we made to Discourse for Ahwaa.

## Plugins

* [Babble](https://github.com/gdpelican/babble): Shoutbox Chat
* [Migrate Password](https://meta.discourse.org/t/migrated-password-hashes-support/19512): Login with old passwords
* [Sidebar Blocks](https://meta.discourse.org/t/discourse-sidebar-blocks/51457): Adding a Sidebar to Discourse
* [Header Search](https://meta.discourse.org/t/header-search-plugin/36435): Add a search bar to the header navigation
* [Retort](https://meta.discourse.org/t/retort-a-reaction-style-plugin-for-discourse/35903): Emoji reactions for topics

Plugins are added as [git subtrees](https://www.atlassian.com/blog/git/alternatives-to-git-submodule-git-subtree), so we can update dependencies just by git pulling.

## Installation

1. Go to `<discourse installation folder>/plugins`
1. `find -type l -delete` to delete old symlinks
1. `find <discourse-ahwaa folder>/plugins -type d -maxdepth 1 -mindepth 1 -exec ln -s '{}' \;` to create new ones

The following plugins have their own repository, run these commands inside the `<discourse installation folder>/plugins` folder to install them.

1. `git clone https://gitlab.com/majalorg/discourse-ahwaa-chat.git`
1. `git clone https://gitlab.com/majalorg/discourse-available-locales.git`
1. `git clone https://gitlab.com/majalorg/discourse-ahwaa-migratepassword.git`

After installing plugins, you need to remove the Rails cache. Go to the Discourse folder (`discourse`) and run `rm -R tmp`

## Data import from old version

The import script needs to be executed from the discourse folder. This needs be run only once.

1. Copy `import_scripts/ahwaa` to `discourse/scripts/import_scripts/ahwaa`
1. Check the script `initialize` method to change mysql2 parameters
1. Run the script with `bundle exec ruby script/import_scripts/ahwaa/ahwaa.rb`.

## Adding a new plugin

1. Add a remote. `git remote add -f  <github user>-<plugin name> https://github.com/<github user>/<plugin name>.git`
2. Add plugin with subtree command. `git subtree add --prefix plugins/<plugin-name> <github user>-<plugin name> master --squash`.

**ex:**

```sh
git remote add -f gdpelican-babble https://github.com/gdpelican/babble.git
git subtree add --prefix plugins/babble gdpelican-babble master --squash
```

## Rebuilding assets in a server

There are cases where want to restart the a staging server in order to see new changes. Do to this you need to

1. Get inside the server.
1. Go to the discourse folder. `cd /opt/discourse`
1. Get into the Discourse docker container. `./launcher enter web`
1. Stop unicorn. `sv stop unicorn`
1. Compile new assets `su discourse -c 'bundle exec rake assets:precompile'`
1. Start unicorn. `sv start unicorn`

## Using the import script

1. Check the `import_scripts/ahwaa/ahwaa.rb` file and replace configuration files at the top
1. Copy the `import_scripts/ahwaa` directory to `<discourse_folder>/script/import_scripts`
1. From the Discourse folder root, run `bundle exec ruby script/import_scripts/ahwaa/ahwaa.rb`

If you are getting errors related to gems missing, comment in the
Gemfile `if ENV['IMPORT'] = 1`

## How to push data from local database to a server

The process to do this is

1. Create a backup locally. `http://localhost:3000/admin/backups`
1. In the server, go to Discourse admin settings and search for the `disable emails` settings and enable it.
1. Upload this backup to the server
1. SSH to the server and enter the Discourse docker container. You can do this by going to the `/opt/discourse` folder and running
`./launcher enter web`
1. Inside the container run `discourse enabled_restore` and `discourse restore`, then follow instructions
