# discourse-ahwaa-related-topics

Plugin that replace the default layout of "suggested topic list" within each of the topics. This plugin only replace the `/discourse/templates/components/suggested-topics.hbs` The other replacements and code over the core of discourse used by this plugin is handled by `discourse-ahwaa-topics`

![topic_list](./doc/related_topics.png)

# Contributors

[Alexis Duran](https://github.com/duranmla)
