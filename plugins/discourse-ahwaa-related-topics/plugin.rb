# name: discourse-ahwaa-related-topics
# about: A Discourse plugin that replace the suggested topics layout
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-related-topics

register_asset 'stylesheets/main.scss'
