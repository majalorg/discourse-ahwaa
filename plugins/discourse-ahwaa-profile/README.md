# discourse-ahwaa-profile

This plugin is intended to overwrite styles and UI that user will find on the profile pages for its own user and others users

## Disclaimer

In order to be able to achieve the full customization of the "others profile" page, we need to overwrite the following files:

* `templates/user/summary.hbs` so we will be able to remove wrappers over listings and correctly display them one on a side of the other as expected, besides, we have full control to change the order that they are being displayed.
* `templates/components/user-summary-topic.hbs` which is the one used by "Top replies" and "Top Topics" listing to render data
* `templates/components/user-summary-user.hbs` which is the one used by "Most replied to", "Most liked by" and "Most liked" listings to render data.

# Contributors

[Alexis Duran](https://github.com/duranmla)
