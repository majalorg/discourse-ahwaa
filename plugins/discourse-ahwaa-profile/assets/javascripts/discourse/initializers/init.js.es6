import { withPluginApi } from "discourse/lib/plugin-api";
import { on } from "ember-addons/ember-computed-decorators";

export default {
  name: "profile-init",
  initialize(container) {
    var ObjectPromiseProxy = Ember.ObjectProxy.extend(Ember.PromiseProxyMixin);

    withPluginApi("0.8.9", api => {
      api.onPageChange(() => {
        let target = $(".user-main .about .username");

        if (!target.length || !$(":root").hasClass("mobile-view")) return;

        target.addClass("clamp-text-1");
      });

      api.modifyClass("component:user-stream-item", {
        isUncategorizedCategory: Ember.computed(function() {
          return this.item.category.slug === "uncategorized";
        })
      });

      // TODO: the approach here idle should be to make sure the created_at property gets the right value as will be expected
      // while the value is not present we will make a request for every user to check the created_at time
      api.modifyClass("component:user-summary-user", {
        memberSinceYear: Ember.computed(function() {
          const { user } = this.attrs;

          return ObjectPromiseProxy.create({
            promise: this.store
              .find("user", user.username)
              .then(user => moment(user.created_at).format("YYYY"))
          });
        })
      });

      // TODO: we need to find the way to fetch for the topic author data in order to render its avatar
      api.modifyClass("component:user-summary-topic", {
        topicAuthor: Ember.computed(function() {
          return "";
        })
      });
    });
  }
};
