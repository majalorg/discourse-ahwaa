# name: ahwaa-profile
# about: A Discourse plugin that adds Ahwaa styling base
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-profile

register_asset 'stylesheets/main.scss'
register_asset 'stylesheets/desktop/main.scss', :desktop
register_asset 'stylesheets/mobile/main.scss', :mobile
