# name: discourse-ahwaa-discussion
# about: A Discourse plugin that adds Ahwaa styling for discussion page
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-discussion

register_asset 'stylesheets/main.scss'
register_asset 'stylesheets/mobile/main.scss', :mobile
