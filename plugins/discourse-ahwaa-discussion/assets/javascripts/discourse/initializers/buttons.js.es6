import { withPluginApi } from "discourse/lib/plugin-api";
import { on } from "ember-addons/ember-computed-decorators";

export default {
  name: "docked-topic-buttons-animation",
  initialize(container) {
    const AnimationHandler = {
      // we need a namespaced event in order to be able to remove them
      eventName: "docked-topic-buttons",
      // space to keep the docked buttons a little separated from header on docked
      buffer: 15,
      // flag to bind the watch scroll event or not
      shouldAddWatchers: false,
      // flag to avoid attach the event twice
      eventsAttached: false,
      // variable to centralize management of class to animate the target
      classToAdd: "docked",

      // we catch all the offsets and bounderies needed to trigger the animation as expected
      _bindElements() {
        let isMobileView = $(":root").hasClass("mobile-view");
        this.$target = $("#topic-footer-buttons");
        this.$targetParent = this.$target.parents(".topic-main-wrapper");

        if (!this.$target.length || isMobileView) {
          this.shouldAddWatchers = false;
          return;
        }

        // we need to prevent the $target to be outside its natural render position before compute any offset
        this._resetTargetPosition(this.$target);

        const targetParentLeftPad = parseFloat(
          this.$targetParent.css("padding-left")
        );

        this.headerHeight = $("header.d-header").outerHeight(true);
        this.targetOffset = this.$target.offset();
        this.parentOffset = this.$targetParent.offset();
        this.totalParentOffsetLeft =
          targetParentLeftPad + this.parentOffset.left;
        this.triggerPoint =
          this.targetOffset.top - (this.headerHeight + this.buffer);
        this.shouldAddWatchers = true;
      },

      _resetTargetPosition(target) {
        this.$target.css({ left: "", top: "" }).removeClass(this.classToAdd);
      },

      _updateTargetPosition(willDock) {
        this.$target
          .css({
            left: willDock ? this.totalParentOffsetLeft : "",
            top: willDock ? this.headerHeight + this.buffer : ""
          })
          .toggleClass(this.classToAdd, willDock);
      },

      _addWatchers() {
        if (this.eventsAttached) {
          return;
        }

        $(window).on(`resize.${this.eventName}`, () => this._bindElements());

        $(window).on(`scroll.${this.eventName}`, () => {
          const scrolled = $(document).scrollTop();
          const willDock = scrolled >= this.triggerPoint;

          this._updateTargetPosition(willDock);
        });

        this.eventsAttached = true;
      },

      _destroyWatchers() {
        $(window).off(`scroll.${this.eventName}`);
        $(window).off(`resize.${this.eventName}`);
        this.eventsAttached = false;
      },

      init() {
        this._bindElements();
        if (this.shouldAddWatchers) {
          this._addWatchers();
        } else {
          this._destroyWatchers();
        }
      }
    };

    withPluginApi("0.8.9", api => {
      api.onPageChange(() => {
        AnimationHandler.init();
      });
    });
  }
};
