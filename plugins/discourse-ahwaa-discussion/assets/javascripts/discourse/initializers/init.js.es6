import { avatarFor } from "discourse/widgets/post";
import { formatUsername } from "discourse/lib/utilities";
import { h } from "virtual-dom";
import { numberNode } from "discourse/helpers/node";
import { withPluginApi } from "discourse/lib/plugin-api";
import { dateNode } from "discourse/helpers/node";

export default {
  name: "discussion-init",
  initialize(container) {
    withPluginApi("0.8.9", api => {
      api.reopenWidget("deleted-post", {
        html(attrs) {
          return [
            h("span.mr-2", I18n.t("ahwaa.deleted_by")),
            " ",
            avatarFor.call(this, "small", {
              template: attrs.deletedByAvatarTemplate,
              username: attrs.deletedByUsername
            }),
            " ",
            dateNode(attrs.deleted_at)
          ];
        }
      });

      // customize post-menu actions to have buttons with text label
      api.reopenWidget("post-menu", {
        html(attrs, state) {
          // WARNING: this will avoid to show the `show-more` button
          this.settings.collapseButtons = false;

          // check post-menu implementation to understand this approach as there is no other way to access to the buttons
          let node = this._super(attrs, state)[0];
          // we take the lastest child cause for cases where posts have replies all controls will be there otherwise children will have a single item and it will work too
          let postControls = _.last(node.children) || {};
          let postControlsActions = postControls.children || [];
          let customizedButtons = postControlsActions.map(child => {
            // if child is different to a widget it won't have an attrs property
            if (child.attrs) {
              child.attrs.label =
                child.attrs.label || `${child.attrs.title}_short`;
            } else {
              // NOTE: as we could verify for some other nodes such the like button, it looks like the logic gets broken afterwards
            }

            return child;
          });

          return node;
        }
      });

      // listen for the event that insert .extra-info-wrapper within header to appy a class that allow us to style the header properly
      api.onAppEvent("topic:current-post-scrolled", () => {
        const $header = $(".d-header");

        if ($header.find(".extra-info-wrapper").get(0)) {
          $header.addClass("has-extra-info-wrapper");
        } else {
          $header.removeClass("has-extra-info-wrapper");
        }
      });

      // Make sure reply section within each of the topic posts show the design layout
      api.reopenWidget("reply-to-tab", {
        html(attrs, state) {
          if (state.loading) {
            return I18n.t("loading");
          }
          const replyUsername = formatUsername(attrs.replyToUsername);

          return h("span", [
            I18n.t("ahwaa.replied_to"),
            h("span", { className: "reply-username" }, `${replyUsername}`)
          ]);
        }
      });

      // attach topic-map always
      api.reopenWidget("post-body", {
        html(attrs, state) {
          let result = this._super(attrs, state);

          if (attrs.post_number === 1) {
            // we want to attach a custom version of the map within the first post within the topic
            result.push(this.attach("custom-topic-map", attrs));
          }

          return result;
        }
      });

      // customize the topic-map
      api.reopenWidget("topic-map-summary", {
        html(attrs, state) {
          const contents = [];

          contents.push(
            h("li.replies-info", [
              h("i.icon.icon-icn_19bubbles"),
              numberNode(attrs.topicReplyCount)
            ])
          );

          contents.push(
            h("li.secondary.topics-views-info", [
              h("i.icon.icon-icn_20persontick"),
              numberNode(attrs.topicViews, { className: attrs.topicViewsHeat })
            ])
          );

          const nav = h(
            "nav.buttons",
            this.attach("button", {
              title: "topic.toggle_information",
              icon: state.collapsed ? "chevron-down" : "chevron-up",
              action: "toggleMap",
              className: "btn"
            })
          );

          return [nav, h("ul.clearfix", contents)];
        }
      });
    });
  }
};
