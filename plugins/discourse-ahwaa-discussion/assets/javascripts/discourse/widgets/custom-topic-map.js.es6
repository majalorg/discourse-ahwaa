import { createWidget } from 'discourse/widgets/widget';
import { h } from 'virtual-dom';

// we basically just wrap up an already built-in widget to meet our needs
export default createWidget('custom-topic-map', {
  tagName: 'div.customized-topic-map',

  html(attrs, state) {
    return h('div.topic-map.customized', this.attach('topic-map-summary', attrs));
  }
});
