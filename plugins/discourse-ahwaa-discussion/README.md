# discourse-ahwaa-discussion

This plugin will inject styles and markup needed in order to render the discussion page as expected. We need to overwrite the `topic.hbs` template to add some extra markup to wrap all the layout and inject components as the avatar composer, besides, throught a special setting called `sidebar_on_discussion_enable` we will be able to control wether or not the sidebar will be displayed within this page.

In addition to the mentioned above, we are adding heavy CSS customization that pretend to style elements as expected by the design and as an easy approach to hide some other elements, keep in mind widgets such as: `embedded-posts`, `post-stream`, `topic-post`, `topic-avatar`, `topic-meta-data`, `topic-map`, `post-menu-area` as one of the main elements to be customized _(for more information check "stylesheets/" folder)_. Besides, using the discourse API we are `reopenWidget`s in our initializer file, those reopen widgets are: `reply-to-tab`, `post-body`, `topic-map-summary` which basically allow us to change the rendered markup or add markup on top of it.

Lastly, this plugin expect to listen the `topic:current-post-scrolled` app event in order to add special styles within the header whenever gets scrolled in a topic page.

# Contributors

[Alexis Duran](https://github.com/duranmla)
