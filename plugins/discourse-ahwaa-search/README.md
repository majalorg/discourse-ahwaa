# discourse-search

This plugin is intended to overwrite styles and UI that user will find within the advanced search page

## Disclaimer

In order to be able to achieve the full customization of the "advanced search" page, we need to overwrite the following files:

* `templates/full-page-search.hbs` so we will be able to add the page title and customize the topic list items from all results in a better way.

# Contributors

[Alexis Duran](https://github.com/duranmla)
