# name: discourse-ahwaa-anonymous
# about: A Discourse plugin to extend the anonymous functionality
# version: 1.0.0
# authors: Ahwaa team
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-anonymous

# This is required when using add_to_serializer, and needs to be true
enabled_site_setting :ahwaa_anonymous_extensions_enabled

after_initialize do
  if SiteSetting.ahwaa_anonymous_extensions_enabled
    module AnonymousShadowCreatorExtensions
      # same method as core
      # just keeping user's trust level in anonymous user
      def create_shadow(user)
        username = UserNameSuggester.suggest(I18n.t(:anonymous).downcase)

        User.transaction do
          shadow = User.create!(
            password: SecureRandom.hex,
            email: "#{SecureRandom.hex}@anon.#{Discourse.current_hostname}",
            skip_email_validation: true,
            name: username, # prevents error when names are required
            username: username,
            active: true,
            trust_level: user.trust_level,
            manual_locked_trust_level: user.manual_locked_trust_level,
            approved: true,
            approved_at: 1.day.ago,
            created_at: 1.day.ago # bypass new user restrictions
          )

          shadow.user_option.update_columns(
            email_private_messages: false,
            email_digests: false
          )

          shadow.email_tokens.update_all(confirmed: true)
          shadow.activate

          # can not hold dupes
          UserCustomField.where(user_id: user.id, name: 'shadow_id').destroy_all

          UserCustomField.create!(user_id: user.id, name: 'shadow_id', value: shadow.id)
          UserCustomField.create!(user_id: shadow.id, name: 'master_id', value: user.id)

          shadow.reload
          user.reload

          shadow
        end
      end
    end

    AnonymousShadowCreator.singleton_class.prepend AnonymousShadowCreatorExtensions
  end
end
