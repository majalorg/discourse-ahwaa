# discourse-ahwaa-header

This plugin overwrites the `header` core widget to being able to add all the markup needed for the customization. Within the customization that has been done we have reopended several widgets such as: `user-menu`, `user-dropdown`, `header-icons`, `header-buttons` and `header-dropdown`.

One of the most confused changes is about the `user-dropdown` which in desktop we will return only a link as the dropdown is not there in the design but it is present for mobile devices. In order to change the default user dropdown we have used the current dropdown logic changing only the markup returned by `user-menu`, as it is shown at https://github.com/discourse/discourse/blob/a083f31216673c74333e6397230e3c847b739114/app/assets/javascripts/discourse/widgets/user-menu.js.es6#L126 the "panelContents" method is used to deliver the markup that is shown once the `user-dropdown` gets visible showing the `user-menu`, besides, that toggle action is handled by: https://github.com/discourse/discourse/blob/ee9be65b2c8e22d734459877694cc4171e4e5ea1/app/assets/javascripts/discourse/widgets/header.js.es6#L340 and remains untouched.

# Contributors

- [Alexis Duran](https://github.com/duranmla)
- [Orlando Del Aguila](https://github.com/orlando)
