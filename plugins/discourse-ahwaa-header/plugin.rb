# name: discourse-ahwaa-header
# about: A Discourse plugin that makes the Ahwaa header
# version: 1.0.0
# authors: Orlando Del Aguila, Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-header

register_asset 'stylesheets/main.scss'
register_asset 'stylesheets/mobile/main.scss', :mobile
