import { withPluginApi } from "discourse/lib/plugin-api";

export default {
  name: "discussion-composer",
  initialize(container) {
    withPluginApi("0.8.9", api => {
      api.decorateWidget("header-contents:after", helper => {
        const currentUser = helper.widget.currentUser;

        let options = {
          label: "ahwaa_header.start_discussion",
          className: "btn-primary btn-header-cta discussion-composer-button",
          action: currentUser ? "openComposer" : "showLogin"
        };

        return helper.attach("button", options);
      });
    });
  }
};
