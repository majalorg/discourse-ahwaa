import { h } from "virtual-dom";
import { on } from "ember-addons/ember-computed-decorators";
import { withPluginApi } from "discourse/lib/plugin-api";

export default {
  name: "ahwaa-header-init",
  initialize(container) {
    withPluginApi("0.8.9", api => {
      function applyKFormat(number) {
        return number > 999 ? `${(number / 1000).toFixed(1)}k` : number;
      }

      api.decorateWidget("user-menu-links:before", helper => {
        const user = helper.widget.currentUser;

        return h("div.before-menu-links-row", [
          h("div.username", `${user.username}`),
          // TODO: change topic_count for score
          h("div.score", `(${applyKFormat(user.topic_count)})`)
        ]);
      });

      // we need to customize the user menu so it will meet the design requirements
      api.reopenWidget("user-menu", {
        html(attrs, state) {
          // to overcome default settings
          this.settings.maxWidth = 250;
          return this._super(attrs, state);
        },

        panelContents() {
          const user = this.currentUser;
          const path = user.get("path");

          return [
            this.attach("user-menu-links", { path }),
            this.attach("header-profile-overview", { user }),
            this.attach("user-notifications", { path }),
            this.attach("ahwaa-user-menu")
          ];
        }
      });

      api.reopenWidget("header-notifications", {
        html(attrs, state) {
          // NOTE: this will avoid the read_first_notification onboarding to be displayed
          this.currentUser.set("read_first_notification", true);
          return this._super(attrs, state);
        }
      });

      // we are just re-implementing the user-notifications in order to beeen able to set a limit properly for notifications
      api.reopenWidget("user-notifications", {
        refreshNotifications(state) {
          if (this.loading) {
            return;
          }

          let limit = 5;
          if (this.site.mobileView) {
            limit = 3;
          }

          const stale = this.store.findStale(
            "notification",
            { recent: true, limit },
            { cacheKey: "recent-notifications" }
          );

          if (stale.hasResults) {
            const results = stale.results;
            let content = results.get("content");

            // we have to truncate to limit, otherwise we will render too much
            if (content && content.length > limit) {
              content = content.splice(0, limit);
              results.set("content", content);
              results.set("totalRows", limit);
            }

            state.notifications = results;
          } else {
            state.loading = true;
          }

          stale
            .refresh()
            .then(notifications => {
              this.currentUser.set("unread_notifications", 0);
              state.notifications = notifications;
            })
            .catch(() => {
              state.notifications = [];
            })
            .finally(() => {
              state.loading = false;
              state.loaded = true;
              this.sendWidgetAction("notificationsLoaded", {
                notifications: state.notifications,
                markRead: () => this.markRead()
              });
              this.scheduleRerender();
            });
        }
      });

      // Customize the icons within the header panel under navigation list
      api.reopenWidget("header-icons", {
        toggleCategoryContainer() {
          let className = "category-bar-toggled";
          let $main = $("#main-outlet");
          let $target = $(":root");

          $main.off("click.categoryToggler");

          if (!$target.hasClass(className)) {
            $main.one("click.categoryToggler", () =>
              $target.toggleClass(className)
            );
          }

          $target.toggleClass(className);
        },

        html(attrs, state) {
          let icons = this._super(attrs, state) || [];

          // we take the icons except the hamburger as we don't want it
          icons = _.reject(
            icons,
            i => i.attrs.iconId === "toggle-hamburger-menu"
          );

          if (this.site.mobileView) {
            // push the tags icon for movileView, use li to match discourse html structure
            icons.push(
              h("li.header-tag-icon", [
                this.attach("flat-button", {
                  action: "toggleCategoryContainer",
                  className: "category-toggle-trigger"
                }),
                h("i.icon.icon-icn_08tag")
              ])
            );
          }

          return icons;
        }
      });

      // a practical way to avoid to render header-buttons
      api.reopenWidget("header-buttons", {
        html(attrs) {
          const buttons = [];
          const user = this.currentUser;

          if (!user && attrs.canSignUp && !attrs.topic) {
            buttons.push(
              this.attach("button", {
                label: "sign_up",
                className: "btn-header sign-up-button",
                action: "showCreateAccount"
              })
            );

            buttons.push(
              this.attach("button", {
                label: "log_in",
                className: "btn-header login-button",
                action: "showLogin"
              })
            );
          }

          return buttons;
        }
      });

      // we need to completly overwrite the header-dropdown#html in order to update its icon
      api.reopenWidget("header-dropdown", {
        buildClasses() {
          return "header-dropdown-container";
        },

        html(attrs) {
          const title = I18n.t(attrs.title);

          if (attrs.contents) {
            body.push(attrs.contents.call(this));
          }

          return h(
            "a.icon.btn-flat",
            {
              attributes: {
                href: attrs.href,
                "data-auto-route": true,
                title,
                "aria-label": title,
                id: attrs.iconId
              }
            },
            h("i.icon.icon-icn_02search")
          );
        }
      });
    });
  }
};
