import { createWidget } from 'discourse/widgets/widget';
import hbs from 'discourse/widgets/hbs-compiler';

createWidget('header-profile-overview', {
  tagName: 'div.profile-overview',
  template: hbs`
    <div class="user-progress">
      <div class="progress-bar-container">
        <div class="progress-bar" style="width: 75%;"></div>
      </div>
      <div class="profile-overview-icon">
        <i class="icon icon-icn_03heart d-icon d-icon-heart"></i>
      </div>
    </div>
  `
});
