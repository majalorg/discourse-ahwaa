import { CREATE_TOPIC } from "discourse/models/composer";
import { createWidget } from "discourse/widgets/widget";
import hbs from "discourse/widgets/hbs-compiler";

createWidget("header-contents", {
  tagName: "div.contents",
  template: hbs`
    {{#if attrs.topic}}
      {{attach widget="header-topic-info" attrs=attrs}}
    {{/if}}
    <div class="title-wrap">
      {{attach widget="home-logo" attrs=attrs}}
      {{attach widget="ahwaa-language-selector"}}
    </div>
    <div class="panel">{{yield}}</div>
  `,

  buildClasses(attrs) {
    // allow to show the logo at first even when the sorting is different within the DOM
    return attrs.topic ? "contents-reversed" : "";
  },

  openComposer() {
    // NOTE: this shouldn't happen as we check to render the button with this action only for logged users
    if (!this.currentUser) {
      return;
    }

    let composer = Discourse.__container__.lookup("controller:composer");

    // REVIEW: There is a odd delay to open the composer, the approach described at https://meta.discourse.org/t/open-composer-with-prefilled-information/82915/3
    composer.open({
      action: CREATE_TOPIC,
      draftKey: "new_topic"
    });
  }
});
