import { createWidget } from 'discourse/widgets/widget';
import hbs from 'discourse/widgets/hbs-compiler';

export default createWidget('ahwaa-language-selector', {
  tagName: 'div.side-logo-container',
  buildKey: () => `language-selector`,
  template: hbs`
    <div class="language-selector">
      <a href="/arabic-version-url" class="language-selector-link">
        النسخة العربية
      </a>
    </div>
  `
});
