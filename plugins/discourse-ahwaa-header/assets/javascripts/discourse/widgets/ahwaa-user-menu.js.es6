import { createWidget } from "discourse/widgets/widget";
import { h } from "virtual-dom";

createWidget("ahwaa-user-menu", {
  tagName: "div.ahwaa-user-menu",
  html() {
    const { currentUser, siteSettings } = this;

    const isAnon = currentUser.is_anonymous;
    const allowAnon =
      (siteSettings.allow_anonymous_posting &&
        currentUser.trust_level >=
          siteSettings.anonymous_posting_min_trust_level) ||
      isAnon;

    const path = currentUser.get("path");
    let links = [];

    if (allowAnon) {
      const action = "toggleAnonymous";
      let label = "switch_to_anon";
      let className = "enable-anonymous";

      if (isAnon) {
        label = "switch_from_anon";
        className = "disable-anonymous";
      }

      links.push(
        h(
          "li.custom-dropdown-item",
          this.attach("flat-button", {
            action,
            label,
            className
          })
        )
      );
    }

    links.push(
      h(
        "li.custom-dropdown-item",
        this.attach("link", {
          href: `${path}/activity/bookmarks`,
          label: "ahwaa_header.dropdown.bookmarks"
        })
      )
    );

    links.push(
      h(
        "li.custom-dropdown-item",
        this.attach("link", {
          href: `${path}/messages`,
          label: "ahwaa_header.dropdown.inbox"
        })
      )
    );

    links.push(
      h(
        "li.custom-dropdown-item",
        this.attach("link", {
          href: `${path}/preferences/account`,
          label: "ahwaa_header.dropdown.preferences"
        })
      )
    );

    if (this.site.mobileView) {
      links.push(
        h(
          "li.custom-dropdown-item",
          this.attach("flat-button", {
            action: "languageSwitch",
            label: "ahwaa_header.dropdown.language_switcher"
          })
        )
      );
    }

    links.push(
      h(
        "li.custom-dropdown-item",
        this.attach("link", {
          href: `mailto:${siteSettings.supportEmail}`,
          label: "ahwaa_header.dropdown.help"
        })
      )
    );

    links.push(
      h(
        "li.custom-dropdown-item",
        this.attach("flat-button", {
          action: "logout",
          label: "ahwaa_header.dropdown.logout"
        })
      )
    );

    return h("ul.custom-dropdown", links);
  }
});
