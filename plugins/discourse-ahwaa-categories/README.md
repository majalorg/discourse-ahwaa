# discourse-ahwaa-categories

Plugin to insert the component that will render the available categories in the community platform. It uses the :after helper on `decorateWidget` over `header` widget to allocate the component as the design suggests.

![categories_bar](./doc/categories_bar.png)

# Contributors

[Alexis Duran](https://github.com/duranmla)
