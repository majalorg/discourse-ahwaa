import { createWidget } from 'discourse/widgets/widget';
import hbs from 'discourse/widgets/hbs-compiler';

createWidget('category-bar', {
  tagName: 'div.category-bar-container',
  template: hbs`
  <ul id="header-tags" class="category-bar">
    <li class="category-bar-item">
      <a class="category-bar-link" href="/c/sexuality">
        {{i18n "ahwaa_categories.sexuality"}}
      </a>
    </li>
    <li class="category-bar-item">
      <a class="category-bar-link" href="/c/general">
        {{i18n "ahwaa_categories.general"}}
      </a>
    </li>
    <li class="category-bar-item">
      <a class="category-bar-link" href="/c/personal">
        {{i18n "ahwaa_categories.personal"}}
      </a>
    </li>
    <li class="category-bar-item">
      <a class="category-bar-link" href="/c/identity">
        {{i18n "ahwaa_categories.identity"}}
      </a>
    </li>
    <li class="category-bar-item">
      <a class="category-bar-link" href="/c/society">
        {{i18n "ahwaa_categories.society"}}
      </a>
    </li>
    <li class="category-bar-item">
      <a class="category-bar-link" href="/c/religion">
        {{i18n "ahwaa_categories.religion"}}
      </a>
    </li>
    <li class="category-bar-item">
      <a class="category-bar-link" href="/c/family">
        {{i18n "ahwaa_categories.family"}}
      </a>
    </li>
    <li class="category-bar-item">
      <a class="category-bar-link" href="/c/relationships">
        {{i18n "ahwaa_categories.relationships"}}
      </a>
    </li>
    <li class="category-bar-item">
      <a class="category-bar-link" href="/c/culture">
        {{i18n "ahwaa_categories.culture"}}
      </a>
    </li>
  </ul>
  `,
});

/*
 // FIXME: Attach a widget within another widget
  <div class="category-bar-link dropdown-structure">
    {{categories-bar-dropdown}}
  </div>
*/
