import { withPluginApi } from 'discourse/lib/plugin-api';

export default {
  name: 'categories',
  initialize(container){
    withPluginApi('0.8.9', api => {
      api.decorateWidget('header:after', function(helper) {
        return helper.attach('category-bar');
      });
    });
  }
};
