import DropdownSelectBoxComponent from "select-kit/components/dropdown-select-box";

export default DropdownSelectBoxComponent.extend({
  pluginApiIdentifiers: ["categories-bar-dropdown"],
  classNames: "categories-bar-dropdown",
  title: I18n.t("ahwaa_categories.more"),
  showFullTitle: true,
  allowInitialValueMutation: false,
  headerIcon: ["caret-down"],

  autoHighlight() {},

  computeContent() {
    const items = [
      { id: "homophobia", name: I18n.t("ahwaa_categories.dropdown.homophobia") },
      { id: "introductions", name: I18n.t("ahwaa_categories.dropdown.introductions") },
      { id: "events", name: I18n.t("ahwaa_categories.dropdown.events") },
      { id: "creativity", name: I18n.t("ahwaa_categories.dropdown.creativity") },
      { id: "transexuality", name: I18n.t("ahwaa_categories.dropdown.transexuality") },
      { id: "media", name: I18n.t("ahwaa_categories.dropdown.media") },
      { id: "francais", name: I18n.t("ahwaa_categories.dropdown.francais") },
      { id: "personal", name: I18n.t("ahwaa_categories.dropdown.personal") }
    ];

    return items;
  },

  mutateValue(id) {
    // use the id from the computeContent items to do an action
  }
});
