# name: discourse-ahwaa-categories
# about: A Discourse plugin that implements Ahwaa categories bar
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-categories

register_asset 'stylesheets/main.scss'
