# name: ahwaa
# about: A Discourse plugin that adds Ahwaa styling base
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/ahwaa

register_asset 'stylesheets/bootstrap-grid.min.css'
register_asset 'stylesheets/main.scss'

register_asset 'fonts/Graphik-Medium-Web.eot'
register_asset 'fonts/Graphik-Medium-Web.woff'
register_asset 'fonts/Graphik-Medium-Web.woff2'
register_asset 'fonts/Graphik-Regular-Web.eot'
register_asset 'fonts/Graphik-Regular-Web.woff'
register_asset 'fonts/Graphik-Regular-Web.woff2'
register_asset 'fonts/Graphik-RegularItalic-Web.eot'
register_asset 'fonts/Graphik-RegularItalic-Web.woff'
register_asset 'fonts/Graphik-RegularItalic-Web.woff2'
register_asset 'fonts/Graphik-Semibold-Web.eot'
register_asset 'fonts/Graphik-Semibold-Web.woff'
register_asset 'fonts/Graphik-Semibold-Web.woff2'
register_asset 'fonts/Icons.eot'
register_asset 'fonts/Icons.ttf'
register_asset 'fonts/Icons.woff'

after_initialize do
  # Monkey patch ActiveModel::Serializer to allow us
  # reload child serializers attributes after parent is modified
  class ::ActiveModel::Serializer
    # Update _attributes with superclass _attributes
    def self.reload
      self._attributes = _attributes.merge(superclass._attributes)
    end
  end
end
