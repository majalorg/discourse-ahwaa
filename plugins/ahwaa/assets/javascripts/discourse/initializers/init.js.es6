import { withPluginApi } from "discourse/lib/plugin-api";
import { on } from "ember-addons/ember-computed-decorators";
import RawHtml from "discourse/widgets/raw-html";

export default {
  name: "base-init",
  initialize(container) {
    withPluginApi("0.8.9", api => {
      api.reopenWidget("home-logo", {
        logo() {
          const logoSVG = new RawHtml({
            html: `
            <div id="site-logo">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 28.6">
                <title>Ahwaa</title>
                <path d="M6.9 20.1c-1.7 0-2 1.4-2 2 0 1.5 1.2 2.1 2.6 2.1 2.3 0 4-.7 4-2.7v-1c0-.2-.2-.4-.4-.4H6.9zm-1.4-6.2c-.2.7-.4.8-1.1.8H1.5c-.4 0-.8-.4-.8-.8 0-3.1 2.9-6 7.7-6 4.8 0 7.9 2.6 7.9 7.2v12.3c0 .4-.4.8-.8.8h-2c-.2 0-.4-.1-.6-.3-.2-.3-.3-.8-.8-.8-.7 0-1.8 1.5-4.8 1.5-4.6 0-7.5-2.4-7.5-6.5 0-2.7 2.3-6.2 6.8-6.2h4c.2 0 .4-.2.4-.4v-.6c0-1.6-1.3-2.4-3.1-2.4-.5-.1-2.1.2-2.4 1.4zM30.7 15.3c0-1.3-1.1-2.4-3-2.4s-3 1.1-3 2.4v12.1c0 .4-.4.8-.8.8h-3.6c-.4 0-.8-.4-.8-.8V.8c.1-.4.5-.8.9-.8H24c.4 0 .8.4.8.8v6.7c0 .7.4 1.1 1.2.8.6-.2 1.2-.5 2.4-.5 4.2 0 7.5 3.2 7.5 7.4v12.1c0 .4-.4.8-.8.8h-3.6c-.4 0-.8-.4-.8-.8v-12zM45.1 20.6c0 .2.4.3.4 0l2.3-11.7c.1-.4.4-.6.8-.6h3.9c.4 0 .7.2.8.6l2.5 11.7c0 .2.4.3.4 0l2.9-11.7c.1-.4.4-.6.8-.6h3.6c.6 0 .9.5.8.9l-5.4 18.3c-.1.4-.4.6-.8.6h-4.3c-.4 0-.7-.2-.8-.6l-2.2-9.2c0-.2-.4-.2-.4 0l-1.9 9.2c-.1.4-.4.6-.8.6h-4.3c-.4 0-.6-.2-.8-.6L37.1 9.2c-.1-.4.1-.9.8-.9h3.6c.4 0 .6.2.8.6l2.8 11.7zM71.8 20.1c-1.7 0-2 1.4-2 2 0 1.5 1.2 2.1 2.6 2.1 2.3 0 4-.7 4-2.7v-1c0-.2-.2-.4-.4-.4h-4.2zm-1.4-6.2c-.2.7-.4.8-1.1.8h-2.9c-.4 0-.8-.4-.8-.8 0-3.1 2.9-6 7.7-6 4.8 0 7.9 2.6 7.9 7.2v12.3c0 .4-.4.8-.8.8h-2c-.2 0-.4-.1-.6-.3-.2-.3-.3-.8-.8-.8-.7 0-1.8 1.5-4.8 1.5-4.6 0-7.5-2.4-7.5-6.5 0-2.7 2.3-6.2 6.8-6.2h4c.2 0 .4-.2.4-.4v-.6c0-1.6-1.3-2.4-3.1-2.4-.5-.1-2.1.2-2.4 1.4zM90.5 20.1c-1.7 0-2 1.4-2 2 0 1.5 1.2 2.1 2.6 2.1 2.3 0 4-.7 4-2.7v-1c0-.2-.2-.4-.4-.4h-4.2zm-1.4-6.2c-.2.7-.4.8-1.1.8h-2.9c-.4 0-.8-.4-.8-.8 0-3.1 2.9-6 7.7-6 4.8 0 7.9 2.6 7.9 7.2v12.3c0 .4-.4.8-.8.8h-2c-.2 0-.4-.1-.6-.3-.2-.3-.3-.8-.8-.8-.7 0-1.8 1.5-4.8 1.5-4.6 0-7.5-2.4-7.5-6.5 0-2.7 2.3-6.2 6.8-6.2h4c.2 0 .4-.2.4-.4v-.6c0-1.6-1.3-2.4-3.1-2.4-.5-.1-2.1.2-2.4 1.4z">
                </path>
              </svg>
            </div>`
          });

          return logoSVG;
        }
      });

      // wait for DOM to be rendered and add some useful classes to the :root
      api.modifyClass("component:site-header", {
        _addUtilitiesClasses() {
          Ember.run.scheduleOnce("afterRender", () => {
            let classes = [];

            if (this.currentUser) {
              classes.push("user-is-logged-in");
            }

            if (this.siteSettings.sidebar_enable) {
              classes.push("sidebar-is-enabled");
            }

            $(":root").addClass(classes.join(" "));
          });
        },

        _updateDocumentLanguage() {
          // by default the html lang attr is agnostic to the real locale displayed
          document.documentElement.lang = I18n.locale;
        },

        @on("didInsertElement")
        baseOnDidInsertElement() {
          this._addUtilitiesClasses();
          this._updateDocumentLanguage();
        }
      });
    });
  }
};
