# ahwaa

This plugin will inject base of styles used across the whole platform, along with generic settings to allow overall modifications such as the font-face of the platform.

Keep in mind that this is the core plugin for all the customization of ahwaa platform works, this folder name should remain as it is in order to be loaded first, all its depended plugins should meet the following criteria for its name: `discourse-ahwaa-<plugin>`.

In order to check the load order of plugins use: `Plugin::Instance.find_all("#{Rails.root}/plugins")` within the rails console, which is how the core of discourse load all plugins or simple type `Dir["#{Rails.root}/plugins/*/plugin.rb"].sort` too also see the sorted plugins.

> Notice that the command to pull the plugins doesn't have the same output as it will be to sort them alphabetically

# Contributors

[Alexis Duran](https://github.com/duranmla)
