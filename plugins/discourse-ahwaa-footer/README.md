# discourse-ahwaa-footer

This plugin will inject the footer for the ahwaa site. Adding the footer using a plugin will avoid the admin to take any extra actions to add markup to the site. Basically, this plugin will load the markup of the footer and append it to the site at the moment of rendering using the same code that would be used the system when using the footer customization.

# Contributors

[Alexis Duran](https://github.com/duranmla)
