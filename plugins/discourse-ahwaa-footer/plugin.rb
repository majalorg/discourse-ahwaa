# name: discourse-ahwaa-footer
# about: A Discourse plugin that adds Ahwaa footer to the site
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-footer

register_asset 'stylesheets/main.scss'
