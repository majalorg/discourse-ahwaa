import { setCustomHTML } from "discourse/helpers/custom-html";

export default {
  name: "base-footer",
  initialize(container) {
    const html = `
      <footer id="main-footer">
        <div class="ahwaa-footer">
          © Ahwaa - All Rights Reserved 2017 |
          <a href="#">Privacy Policy</a> |
          <a href="#">About Us</a>
        </div>
      </footer>
    `;
    setCustomHTML("footer", html);
  }
};
