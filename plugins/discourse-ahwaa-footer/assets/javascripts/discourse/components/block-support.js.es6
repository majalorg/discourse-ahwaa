import Composer from "discourse/models/composer";

export default Ember.Component.extend({
  mailtoUrl: `mailto:${Discourse.SiteSettings.support_email}`,

  _updatePlaceholder() {
    $("#reply-title").attr(
      "placeholder",
      I18n.t("ahwaa.block_support.help_placeholder")
    );
    this.appEvents.off("composer:opened", this, this._updatePlaceholder);
  },

  actions: {
    sendSupportMessage() {
      // REVIEW: check action 'composePrivateMessage' and approach https://meta.discourse.org/t/open-composer-with-prefilled-information/82915/3
      const composer = Discourse.__container__.lookup("controller:composer");
      composer.open({
        action: Composer.PRIVATE_MESSAGE,
        usernames: "admins",
        archetypeId: "private_message",
        draftKey: "new_private_message"
      });

      this.appEvents.on("composer:opened", this, this._updatePlaceholder);
    }
  }
});
