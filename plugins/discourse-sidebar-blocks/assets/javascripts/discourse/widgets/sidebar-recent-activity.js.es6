import { createWidget } from "discourse/widgets/widget";
import { getNotifications } from "discourse/plugins/discourse-sidebar-blocks/discourse/helpers/latest-notifications";
import { getRecentActivity } from "discourse/plugins/discourse-sidebar-blocks/discourse/helpers/recent-activity";
import { h } from "virtual-dom";
import { postUrl } from "discourse/lib/utilities";
import RawHtml from "discourse/widgets/raw-html";

createWidget("sidebar-recent-activity", {
  blockId: "recent_activity",

  entryTypes: {
    LIKED_TYPE: 5,
    INVITED_TYPE: 8,
    SYSTEM: 7,
    GROUP_SUMMARY_TYPE: 16
  },

  tagName: "div.sidebar-recent-activity",

  buildKey: attrs => "sidebar-recent-activity",

  defaultState() {
    return { loadingActivityEntries: false, loadingNotificationEntries: false };
  },

  pullNotificationInformation() {
    if (this.state.loadingNotificationEntries) {
      return;
    }

    this.state.loadingNotificationEntries = true;

    // WARNING: the type will also change the data to be rendered some types may require another type of row and possible new url computation
    const filter = [this.entryTypes.LIKED_TYPE];

    getNotifications(this, filter).then(result => {
      this.state.notificationEntries = result.length ? result : "empty";
      this.state.loadingNotificationEntries = false;
      this.scheduleRerender();
    });
  },

  pullActivityInformation(user) {
    if (this.state.loadingActivityEntries) {
      return;
    }

    this.state.loadingActivityEntries = true;

    getRecentActivity(this, user).then(result => {
      this.state.activityEntries = result.length ? result : "empty";
      this.state.loadingActivityEntries = false;
      this.scheduleRerender();
    });
  },

  sortEntries(entries) {
    return _.sortBy(entries, e => e.created_at).reverse();
  },

  getEntryURLFromAttrs(attrs) {
    const data = attrs.data;

    // NOTE: this code is also present on notification-item widget, will be great to find a way to extend from that widget
    const topicId = attrs.topic_id;

    if (topicId) {
      return postUrl(attrs.slug, topicId, attrs.post_number);
    }

    // Make sure to support the type of notification we want so the url return a useful value
    return "";
  },

  recentActivityRow(data) {
    if (data.notification_type) {
      return this.renderNotificationRow(data);
    } else if (data.action_type) {
      return this.renderActionRow(data);
    } else {
      return null;
    }
  },

  renderNotificationRow(attrs) {
    const { data } = attrs;
    const { topic_title } = data;

    return h(
      "div.ahwaa-sidebar-item.notification-item",
      this.attach("link", {
        href: this.getEntryURLFromAttrs(attrs),
        contents: () =>
          h("div.inner-wrapper", [
            h("div.decorative-image", h("i.icon.icon-icn_13heart")),
            h("div.metadata", [
              h("div.main-data", topic_title),
              h("div.secondary-data", moment(attrs.created_at).fromNow())
            ])
          ])
      })
    );
  },

  renderActionRow(attrs) {
    const { excerpt } = attrs;
    return h(
      "div.ahwaa-sidebar-item.action-item",
      this.attach("link", {
        href: this.getEntryURLFromAttrs(attrs),
        contents: () =>
          h("div.inner-wrapper", [
            h("div.decorative-image", h("i.icon.icon-icn_38chat")),
            h("div.metadata", [
              h("div.main-data", { innerHTML: excerpt }),
              h("div.secondary-data", moment(attrs.created_at).fromNow())
            ])
          ])
      })
    );
  },

  recentActivityHeader() {
    return h(
      "h3.sidebar-heading",
      h(
        "a",
        {
          attributes: {
            href: "/u",
            title: I18n.t("sidebar_blocks.recent_activity")
          }
        },
        I18n.t("sidebar_blocks.recent_activity")
      )
    );
  },

  getAllEntries(state) {
    return state.activityEntries.concat(state.notificationEntries);
  },

  html(attrs, state) {
    const user = this.currentUser;

    if (!user) {
      // without user there is no activity to show
      return null;
    }

    if (!state.activityEntries) {
      // if we have no data attached to "activityEntries" we need to start pull that data
      this.pullActivityInformation(user);
    }

    if (!state.notificationEntries) {
      // if we have no data attached to "notificationEntries" we need to start pull that data
      this.pullNotificationInformation();
    }

    let result = [];

    if (state.loadingActivityEntries || state.loadingNotificationEntries) {
      // one of the two collections is loading
      result.push(h("div.spinner-container", h("div.spinner")));
    } else if (
      state.activityEntries !== "empty" ||
      state.notificationEntries !== "empty"
    ) {
      // if one of the both collections has something we will proceed
      result.push(this.recentActivityHeader());

      let entries = this.sortEntries(this.getAllEntries(state));
      let rows = entries.map(entry => this.recentActivityRow(entry));

      result.push(h("div.directory", rows));
    } else {
      // simple not display anything if nothing was found
      result = h("span.empty-content");
    }

    this.dispatchNotification(
      !state.loadingActivityEntries && !state.loadingNotificationEntries
    );
    return result;
  },

  dispatchNotification(dispatch) {
    if (!dispatch) {
      return;
    }

    this.appEvents.trigger(`sidebar-blocks:${this.blockId}.loaded`);
  }
});
