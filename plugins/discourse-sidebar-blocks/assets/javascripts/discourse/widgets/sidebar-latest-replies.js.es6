import { createWidget } from 'discourse/widgets/widget';
import { getLatestReplies } from 'discourse/plugins/discourse-sidebar-blocks/discourse/helpers/recent-replies';
import { h } from 'virtual-dom';

export default createWidget('sidebar-latest-replies', {
  blockId: 'latest_replies',
  tagName: 'div.sidebar-latest-replies',
  buildKey: attrs => 'sidebar-latest-replies',
  defaultState() {
    return { loading: false };
  },

  refreshPosts() {
    if (this.state.loading) {
      return;
    }
    this.state.loading = true;
    this.state.posts = 'empty';
    getLatestReplies(this).then(result => {
      if (result.length) {
        for (var i = result.length - 1; i >= 0; i--) {
          // remove first post in a topic (not a reply)
          // remove any "post" that is merely an action
          // remove hidden posts
          if (
            result[i].post_number < 2 ||
            result[i].action_code != undefined ||
            result[i].hidden
          ) {
            result.splice(i, 1);
          }
        }

        for (var i = result.length - 1; i >= 0; i--) {
          // limit to 5 max
          if (i > 4) {
            result.splice(i, 1);
          }
        }
        this.state.posts = result;
      } else {
        this.state.posts = 'empty';
      }
      this.state.loading = false;
      this.scheduleRerender();
    });
  },

  html(attrs, state) {
    const messageBus = Discourse.__container__.lookup('message-bus:main');
    messageBus.subscribe('/latest', data => {
      this.refreshPosts();
    });

    if (!state.posts) {
      this.refreshPosts();
    }

    let result = [];

    if (state.loading) {
      result.push(h('div.spinner-container', h('div.spinner')));
    } else if (state.posts !== 'empty') {
      result.push(h('h3.sidebar-heading', I18n.t('sidebar_blocks.recent_replies')));
      const replies = state.posts.map(t => this.attach('sidebar-reply-item', t));
      result.push(replies);
    } else {
      // simple not display anything if nothing was found
      result = h('span.empty-content');
    }

    this.dispatchNotification(!state.loading);
    return result;
  },

  dispatchNotification(dispatch) {
    if (!dispatch) {
      return;
    }

    this.appEvents.trigger(`sidebar-blocks:${this.blockId}.loaded`);
  }
});
