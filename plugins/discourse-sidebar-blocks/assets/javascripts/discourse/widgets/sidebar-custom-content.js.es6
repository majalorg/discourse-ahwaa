import { createWidget } from 'discourse/widgets/widget';
import { h } from 'virtual-dom';

createWidget('sidebar-custom-content', {
  blockId: 'custom_html',
  tagName: 'div.sidebar-custom-content',

  html(attrs) {
    this.dispatchNotification(true);
    return h('div', {
      innerHTML: Discourse.SiteSettings.sidebar_custom_content
    });
  },

  dispatchNotification(dispatch) {
    if (!dispatch) {
      return;
    }

    this.appEvents.trigger(`sidebar-blocks:${this.blockId}.loaded`);
  }
});
