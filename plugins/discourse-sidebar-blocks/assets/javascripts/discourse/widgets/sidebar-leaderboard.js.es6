import { createWidget } from 'discourse/widgets/widget';
import { h } from 'virtual-dom';
import { getLeaderboardList } from 'discourse/plugins/discourse-sidebar-blocks/discourse/helpers/leaderboard-list';

createWidget('sidebar-leaderboard', {
  blockId: 'leaderboard',
  tagName: 'div.sidebar-leaderboard',

  buildKey: attrs => 'sidebar-leaderboard',

  defaultState() {
    return { loading: false };
  },

  refreshUsers() {
    if (this.state.loading) {
      return;
    }

    this.state.loading = true;
    this.state.users = 'empty';

    getLeaderboardList(this).then(result => {
      if (result.length) {
        this.state.users = result;
      } else {
        this.state.users = 'empty';
      }
      this.state.loading = false;
      this.scheduleRerender();
    });
  },

  leaderboardRow(data, position) {
    const { user } = data;
    return h('div.leaderboard-item', [
      h('div.user-avatar', [
        this.attach('topic-participant', user),
        h('div.leaderboard-position', `${position}`)
      ]),
      h('div.metadata', [
        h('div.user-name.trigger-data-card.main-data', user.username),
        h(
          'div.user-name.trigger-data-card.secondary-data',
          `${I18n.t('sidebar_blocks.members_since')} ${moment(user.created_at).format(
            'YYYY'
          )}`
        )
      ])
    ]);
  },

  leaderboardHeader() {
    const title = I18n.t('sidebar_blocks.leaderboard');
    const opts = { attributes: { href: '/u', title } };

    return h('h3.sidebar-heading', h('a', opts, title));
  },

  html(attrs, state) {
    if (!state.users) {
      this.refreshUsers();
    }

    let result = [];

    if (state.loading) {
      result.push(h('div.spinner-container', h('div.spinner')));
    } else if (state.users !== 'empty') {
      result.push(this.leaderboardHeader());

      const users = state.users.map((user, i) => this.leaderboardRow(user, i + 1));

      result.push(h('div.directory', users));
    } else {
      // simple not display anything if nothing was found
      result = h('span.empty-content');
    }

    this.dispatchNotification(!state.loading);
    return result;
  },

  dispatchNotification(dispatch) {
    if (!dispatch) {
      return;
    }

    this.appEvents.trigger(`sidebar-blocks:${this.blockId}.loaded`);
  }
});
