import { createWidget } from 'discourse/widgets/widget';
import { getFollowedTopics } from 'discourse/plugins/discourse-sidebar-blocks/discourse/helpers/followed-topics';
import { h } from 'virtual-dom';
import { postUrl } from 'discourse/lib/utilities';

createWidget('sidebar-followed-topics', {
  blockId: 'followed_topics',
  tagName: 'div.sidebar-followed-topics',

  buildKey: attrs => 'sidebar-followed-topics',

  defaultState() {
    return { loading: false };
  },

  getTopics(result) {
    const limit = +Discourse.SiteSettings.sidebar_followed_topics_count;
    return result.topic_list.topics.slice(-limit).reverse();
  },

  refreshData() {
    if (this.state.loading) {
      return;
    }

    this.state.loading = true;
    this.state.requestResults = 'empty';

    getFollowedTopics(this).then(result => {
      if (this.getTopics(result).length) {
        // NOTE: the result from a request to a topics list also has the users data for the listed topics
        this.state.requestResults = result;
      } else {
        this.state.requestResults = 'empty';
      }
      this.state.loading = false;
      this.scheduleRerender();
    });
  },

  getAuthor(data) {
    // translations for "original_poster" live within sever.<locale>.yml files
    const targetDescription = I18n.t('sidebar_blocks.original_poster');
    let originalPoster = data.posters.filter(poster =>
      _.include(poster.description.toLowerCase(), targetDescription.toLowerCase())
    )[0];

    // edge cases where user is loading an interface language that we are not expected to support rely on the fact that "original poster" will be the first within data.posters array
    if (!originalPoster) {
      originalPoster = data.posters[0];
    }

    return _.filter(
      this.state.requestResults.users,
      u => u.id === originalPoster.user_id
    )[0];
  },

  getEntryURLFromAttrs(attrs) {
    // NOTE: this code is also present on notification-item widget, will be great to find a way to extend from that widget
    const topicId = attrs.id;
    return topicId ? postUrl(attrs.slug, topicId) : '';
  },

  dataRow(attrs) {
    const { title } = attrs;
    const author = this.getAuthor(attrs);
    const secondary = `${I18n.t('sidebar_blocks.posted_by')} ${author.username}`;

    return h(
      'div.ahwaa-sidebar-item.followed-topic-item',
      this.attach('link', {
        href: this.getEntryURLFromAttrs(attrs),
        contents: () =>
          h('div.inner-wrapper', [
            h('div.user-avatar', this.attach('topic-participant', author)),
            h('div.metadata', [
              h('div.main-data', title),
              h('div.secondary-data', secondary)
            ])
          ])
      })
    );
  },

  header() {
    const title = I18n.t('sidebar_blocks.followed_topics');
    const opts = { attributes: { href: '/latest?state=watching', title } };

    return h('h3.sidebar-heading', [
      title,
      h(
        'span.title-action',
        this.attach('link', {
          href: `/latest?state=watching`,
          label: 'sidebar_blocks.view_all'
        })
      )
    ]);
  },

  html(attrs, state) {
    if (!state.requestResults) {
      this.refreshData();
    }

    let result = [];

    if (state.loading) {
      result.push(h('div.spinner-container', h('div.spinner')));
    } else if (state.requestResults !== 'empty') {
      result.push(this.header());
      const topics = this.getTopics(state.requestResults);
      const topicsRows = topics.map((topic, i) => this.dataRow(topic, i + 1));

      result.push(h('div.directory', topicsRows));
    } else {
      // simple not display anything if nothing was found
      result = h('span.empty-content');
    }

    this.dispatchNotification(!state.loading);
    return result;
  },

  dispatchNotification(dispatch) {
    if (!dispatch) {
      return;
    }

    this.appEvents.trigger(`sidebar-blocks:${this.blockId}.loaded`);
  }
});
