import { createWidget } from 'discourse/widgets/widget';
import { h } from 'virtual-dom';

export default createWidget('sidebar-items', {
  tagName: 'div.sidebar-items',
  buildKey: () => 'sidebar-items',
  // pair of blockId and boolean value wether or not the block has been loaded
  defaultState() {
    return {
      blocksLoaded: {},
      eventTriggered: false
    };
  },

  dispatchNotification(eventName) {
    this.state.blocksLoaded[eventName] = true;

    // find any event that hasn't been loaded yet
    const waitForNextBlock =
      _.filter(this.state.blocksLoaded, loaded => !loaded).length > 0;

    // eventTriggered is a way to avoid trigger the event multiple times for the same lifecycle on a single rendering attempt
    if (!waitForNextBlock && !this.state.eventTriggered) {
      this.state.eventTriggered = true;
      this.appEvents.trigger('sidebar-blocks:loaded');
    }
  },

  getEventName(item) {
    return `${item}.loaded`;
  },

  bindToBlocksWatcher(item) {
    let eventName = this.getEventName(item);
    this.state.blocksLoaded[eventName] = this.state.blocksLoaded[eventName] || false;
    this.appEvents.on(`sidebar-blocks:${eventName}`, () =>
      this.dispatchNotification(eventName)
    );
  },

  html(attrs, state) {
    if (!Discourse.SiteSettings.sidebar_enable || this.site.mobileView) return;

    var sidebarBlocks = Discourse.SiteSettings.sidebar_block_order.split('|');

    const result = [];

    sidebarBlocks.map(item => {
      // switch cases are alphabetically sorted
      switch (item) {
        case 'custom_html':
          result.push(this.attach('sidebar-custom-content'));
          this.bindToBlocksWatcher(item);
          break;
        case 'followed_topics':
          // avoid to render any structure is we know there is no user logged in
          if (this.currentUser) {
            result.push(this.attach('sidebar-followed-topics'));
            this.bindToBlocksWatcher(item);
          }
          break;
        case 'latest_replies':
          result.push(this.attach('sidebar-latest-replies'));
          this.bindToBlocksWatcher(item);
          break;
        case 'leaderboard':
          result.push(this.attach('sidebar-leaderboard'));
          this.bindToBlocksWatcher(item);
          break;
        case 'recent_activity':
          // avoid to render any structure is we know there is no user logged in
          if (this.currentUser) {
            result.push(this.attach('sidebar-recent-activity'));
            this.bindToBlocksWatcher(item);
          }
          break;
        case 'support':
          result.push(this.attach('sidebar-support'));
          // NOTE: case of static content that no need of load data do not need to trigger the event
          this.state.blocksLoaded[this.getEventName(item)] = true;
          break;
        default:
          result.push(this.attach('sidebar-category-posts', { category: item }));
          this.bindToBlocksWatcher(item);
      }
    });

    return result;
  }
});
