import Composer from 'discourse/models/composer';
import { createWidget } from 'discourse/widgets/widget';
import { h } from 'virtual-dom';

createWidget('help-button', {
  sendSupportMessage() {
    // REVIEW: check action 'composePrivateMessage' and approach https://meta.discourse.org/t/open-composer-with-prefilled-information/82915/3
    const composer = Discourse.__container__.lookup('controller:composer');
    composer.open({
      action: Composer.PRIVATE_MESSAGE,
      usernames: 'admins',
      archetypeId: 'private_message',
      draftKey: 'new_private_message'
    });

    this.appEvents.on('composer:opened', this, this._updatePlaceholder);
  },

  _updatePlaceholder() {
    $('#reply-title').attr('placeholder', I18n.t('sidebar_blocks.help_placeholder'));
    this.appEvents.off('composer:opened', this, this._updatePlaceholder);
  },

  html() {
    return this.attach('flat-button', {
      action: 'sendSupportMessage',
      label: 'sidebar_blocks.help_action',
      className: 'btn btn-grey'
    });
  }
});

createWidget('sidebar-support', {
  blockId: 'support',
  tagName: 'div.sidebar-support',

  _sendPMButton() {
    return this.attach('help-button');
  },

  _mailtoButton() {
    return this.attach('link', {
      className: 'btn btn-grey',
      label: 'sidebar_blocks.help_action',
      href: `mailto:${Discourse.SiteSettings.sidebar_support_email}`
    });
  },

  html(attrs) {
    const user = this.currentUser;
    return h('div.sidebar-support--inner', [
      h('div.support-image', h('i.icon.icon-icn_25computerquestion')),
      h('div.support-text', h('p', I18n.t('sidebar_blocks.help'))),
      user ? this._sendPMButton() : this._mailtoButton()
    ]);
  }
});
