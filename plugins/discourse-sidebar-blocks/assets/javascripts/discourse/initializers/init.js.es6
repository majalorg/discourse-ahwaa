import { withPluginApi } from "discourse/lib/plugin-api";
import { on } from "ember-addons/ember-computed-decorators";

class SidebarHandler {
  constructor() {
    // we need a namespaced event in order to be able to remove them
    this.eventName = "docked-sidebar-animation";
  }

  // At some point the content within the page may be smaller than the sidebar so we need to ensure the aesthetic of the site for that matter
  _updateMinimunBodyHeight() {
    const sidebarHeight = this.sidebar.outerHeight(true);
    const headerHeight = $("header.d-header").outerHeight(true);

    $("body").css("min-height", sidebarHeight + headerHeight);
  }

  _resetAnimation() {
    // reset inline styles changes added before so the css will take place
    this.sidebar.css({ position: "", top: "" });
  }

  _computeProperties() {
    this.sidebar = $("#sidebar-blocks");

    if (!this.sidebar.length) {
      return;
    }

    this.sidebarItems = this.sidebar.find(".sidebar-items>*");
    this.lastBlock = this.sidebarItems.last();

    const lastBlockOffset = this.lastBlock.offset();

    if (!lastBlockOffset) {
      return;
    }

    // ~300 leave an extra space so the last block and some extra part of the sidebar will be always visible
    const buffer = this.sidebarItems.length > 1 ? 300 : 0;

    this.triggerOffset = lastBlockOffset.top - buffer;

    if (this.triggerOffset < 0) {
      return;
    }

    return true;
  }

  _addSidebarScrollAnimation() {
    // Avoid to apply animation to not solid sidebar
    if (!this.sidebar.hasClass("solid")) {
      return;
    }

    $(window).on(`scroll.${this.eventName}-animation`, () => {
      const scrolled = $(document).scrollTop();
      const sidebarIsFixed = this.sidebar.css("position") === "fixed";

      let addStyles = this.triggerOffset <= scrolled && !sidebarIsFixed;
      let resetStyles = this.triggerOffset > scrolled && sidebarIsFixed;

      if (addStyles) {
        const headerHeight = $("header.d-header").outerHeight(true);
        // as it is the only possible variation after the sidebar gets loaded
        this.sidebar.css({
          position: "fixed",
          top: -(this.triggerOffset - headerHeight)
        });
        return;
      }

      if (resetStyles) {
        this._resetAnimation();
      }
    });
  }

  _cleanElementState() {
    // this declaration is agnostic to the one within _computeProperties
    this.sidebar = $("#sidebar-blocks");

    if (this.sidebar.length) {
      this._resetAnimation();
    }

    $(window).off(`scroll.${this.eventName}-init`);
    $(window).off(`scroll.${this.eventName}-animation`);
  }

  _bindAnimation() {
    // just a double safe prevention to avoid to bind animation twice and clean target styles
    this._cleanElementState();

    // bind animations
    $(window).on(`scroll.${this.eventName}-init`, () => {
      let success = this._computeProperties();

      if (success) {
        this._addSidebarScrollAnimation();
        this._updateMinimunBodyHeight();
        $(window).off(`scroll.${this.eventName}-init`);
      }
    });
  }

  resetBodyHeight() {
    this.sidebar = $("#sidebar-blocks");

    if (this.sidebar.length) {
      return;
    }

    $("body").css("min-height", "");
  }

  init(context) {
    context.appEvents.on("sidebar-blocks:loaded", () => {
      Ember.run.scheduleOnce("afterRender", this._bindAnimation.bind(this));
    });
  }
}

export default {
  name: "sidebar-blocks-init",
  initialize(container) {
    withPluginApi("0.8.9", api => {
      const sidebarHandler = new SidebarHandler();
      api.modifyClass("component:site-header", {
        @on("didInsertElement")
        bindEvents() {
          sidebarHandler.init(this);
        }
      });

      api.onPageChange(() => {
        sidebarHandler.resetBodyHeight();
      });
    });
  }
};
