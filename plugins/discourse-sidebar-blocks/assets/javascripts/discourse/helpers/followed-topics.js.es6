import { ajax } from 'discourse/lib/ajax';

// the endpoint is the same used by the platform at `Show watched topics` option within User > Preferences > Notifications
export function getFollowedTopics(context) {
  return ajax('/latest.json?state=watching')
    .then(result => result)
    .catch(() => {
      return [];
    });
}
