function filterNotificationsByType(content, filter) {
  // WARNING: we do not know the max cap of notifications resolved
  return _.filter(content, item => _.contains(filter, item.notification_type));
}

export function getNotifications(context, filter) {
  const container = Discourse.__container__;
  const store = container.lookup('store:main');
  const limit = 5;

  const stale = store.findStale(
    'notification',
    { recent: true, limit },
    { cacheKey: 'recent-notifications' }
  );

  // NOTE: stale.results may get some content but it doesn't include the whole set of notifications
  return stale
    .refresh()
    .then(notifications => {
      return filterNotificationsByType(notifications.content, filter);
    })
    .catch(() => {
      return [];
    });
}
