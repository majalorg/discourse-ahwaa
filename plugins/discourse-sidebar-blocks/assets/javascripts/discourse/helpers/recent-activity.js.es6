import { ajax } from 'discourse/lib/ajax';

export function getRecentActivity(context, user) {
  const count = Discourse.SiteSettings.sidebar_recent_activity_count;
  const username = user.get('username');

  return ajax(
    `/user_actions.json?offset=0&username=${username}&filter=4,5&no_results_help_key=user_activity.no_default`
  )
    .then(function(result) {
      return result.user_actions.splice(0, count);
    })
    .catch(() => {
      return [];
    });
}
