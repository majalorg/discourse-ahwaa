# discourse-sidebar-blocks

A discourse plugin that adds a sidebar to topic lists (discovery) with several blocks available: posts from a category, recent replies

# Notes

This plugin has been customized to satisfy some special needs of the ahwaa project, in such case as this plugin overwrites the `discovery` template it also depends upon the `ahwaa-hero` plugin that it is rendered within the discovery template, the lack of that plugin will show error within the console.
