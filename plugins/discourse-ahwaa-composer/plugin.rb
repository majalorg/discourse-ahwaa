# name: discourse-ahwaa-composer
# about: A Discourse plugin that adds Ahwaa composer
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-composer

register_asset 'stylesheets/main.scss'
