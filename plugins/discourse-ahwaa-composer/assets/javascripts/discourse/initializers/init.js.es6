import computed from "ember-addons/ember-computed-decorators";
import { on } from "ember-addons/ember-computed-decorators";
import { withPluginApi } from "discourse/lib/plugin-api";
import AvatarBuilder from "discourse/plugins/discourse-ahwaa-avatar/discourse/helpers/avatar";
import RawHtml from "discourse/widgets/raw-html";
import { ajax } from "discourse/lib/ajax";

export default {
  name: "ahwaa-composer-init",
  initialize(container) {
    let avatarBuilder = new AvatarBuilder();

    withPluginApi("0.8.9", api => {
      api.modifyClass("component:d-editor", {
        @on("didInsertElement")
        moveFooterActions() {
          const saveOrCancel = $(".submit-panel .save-or-cancel");
          $(".d-editor-textarea-wrapper").append(saveOrCancel);
        }
      });

      api.modifyClass("model:composer", {
        _initAhwaaTopicType: function() {
          const topicType =
            this.keyValueStore.get("composer.ahwaaTopicType") || "story";
          this.set("ahwaaTopicType", topicType);
        }.on("init"),

        createPost(opts) {
          const composer = this;

          return this._super(opts).then(result => {
            // return if post wasn't created
            // we need a way to handle this, since we cannot update the
            // topic_type until the post is created
            if (result.responseJson.action === "enqueued") {
              return result;
            }

            // return unless the action was create_post
            // we are checking this separately just for visibility
            if (result.responseJson.action !== "create_post") {
              return result;
            }

            // Get ahwaa_topic_type from composer
            const topicType = composer.get("ahwaaTopicType");
            const post = result.responseJson.post;
            const topicId = post.topic_id;

            // Reset topic type selection
            composer.keyValueStore.set({
              key: "composer.ahwaaTopicType",
              value: "story"
            });

            // Make request
            return ajax("/ahwaa_topic_types/update", {
              type: "POST",
              data: {
                topic_id: topicId,
                topic_type: topicType
              }
            })
              .then(result => {})
              .catch(error => {
                throw error;
              });
          });
        }
      });

      api.modifyClass("controller:composer", {
        ahwaaTopicType: Ember.computed("ahwaaTopicType", function() {
          return this.model.get("ahwaaTopicType");
        }),

        actions: {
          ahwaaTopicType(value) {
            this.model.set("ahwaaTopicType", value);

            this.keyValueStore.set({
              key: "composer.ahwaaTopicType",
              value: value
            });
          }
        }
      });

      api.modifyClass("component:composer-toggles", {
        @computed("composeState")
        isComposerOpen(composeState) {
          return composeState === "open";
        }
      });
    });
  }
};
