export default Ember.Component.extend({
  action: null,

  text: Ember.computed("composer", function() {
    const { currentUser } = this;

    return currentUser.is_anonymous ? "switch_from_anon" : "switch_to_anon";
  }),

  allowAnon: Ember.computed("composer", function() {
    const { siteSettings, currentUser } = this;
    const isAnon = currentUser.is_anonymous;

    return (
      (siteSettings.allow_anonymous_posting &&
        currentUser.trust_level >=
          siteSettings.anonymous_posting_min_trust_level) ||
      isAnon
    );
  }),

  actions: {
    click(event) {
      this.sendAction("action");
    }
  }
});
