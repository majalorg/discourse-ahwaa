export default Ember.Component.extend({
  classNames: ["topic-type-toggle"],
  value: null,
  action: null,

  isStory: Ember.computed("value", function() {
    return this.get("value") === "story";
  }),
  isQuestion: Ember.computed("value", function() {
    return this.get("value") === "question";
  }),

  actions: {
    changed(event) {
      const value = event.target.value;
      this.set("value", value);
      this.sendAction("action", value);
    }
  }
});
