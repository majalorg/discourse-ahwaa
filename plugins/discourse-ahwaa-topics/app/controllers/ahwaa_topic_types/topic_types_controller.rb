class AhwaaTopicTypes::TopicTypesController < ApplicationController
  def update
    topic = current_user.topics.find(params[:topic_id])

    topic.custom_fields['ahwaa_topic_type'] = sanitized_topic_type
    topic.save!

    render json: { success: true }
  end

  protected

  # Returns 'story' unless topic_type is valid
  def sanitized_topic_type
    topic_type = params[:topic_type]

    return topic_type if ['question', 'story'].include?(topic_type)

    'story'
  end
end
