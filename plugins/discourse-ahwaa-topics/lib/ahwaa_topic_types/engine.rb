module ::AhwaaTopicTypes
  PLUGIN_NAME = "ahwaa_topic_types".freeze

  class Engine < ::Rails::Engine
    engine_name PLUGIN_NAME
    isolate_namespace AhwaaTopicTypes
  end
end
