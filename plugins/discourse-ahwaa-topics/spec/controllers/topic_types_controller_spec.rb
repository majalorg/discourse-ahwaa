require 'rails_helper'

describe ::AhwaaTopicTypes::TopicTypesController do
  routes { ::AhwaaTopicTypes::Engine.routes }

  let(:user) { log_in }
  let(:topic) { Fabricate(:topic, user: user) }
  let(:other_topic) { Fabricate(:topic) }

  describe "update" do
    context "valid request" do
      it "updates topic_type" do
        expect(topic.custom_fields['ahwaa_topic_type']).to be_nil

        post :update, params: { topic_id: topic.id, topic_type: 'question' }, format: :json

        topic.reload
        expect(topic.custom_fields['ahwaa_topic_type']).to eq('question')
      end
    end

    context "invalid request" do
      it "updates topic_type to 'story' if topic_type is invalid" do
        expect(topic.custom_fields['ahwaa_topic_type']).to be_nil

        post :update, params: { topic_id: topic.id, topic_type: 'non_valid' }, format: :json

        topic.reload
        expect(topic.custom_fields['ahwaa_topic_type']).to eq('story')
      end

      it "raises RecordNotFound if topic doesn't belong to user" do
        expect(topic.custom_fields['ahwaa_topic_type']).to be_nil

        expect { post :update, params: { topic_id: other_topic.id, topic_type: 'question' }, format: :json }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
