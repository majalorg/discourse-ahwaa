# name: discourse-ahwaa-topics
# about: A Discourse plugin that makes the Ahwaa Topics
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-topics

# This is required when using add_to_serializer, and needs to be true
enabled_site_setting :ahwaa_topic_types_enabled

register_asset 'stylesheets/main.scss'

# Load engine
load File.expand_path('lib/ahwaa_topic_types/engine.rb', __dir__)

after_initialize do
  # Append routes to the Discourse app, by mounting the Engine
  Discourse::Application.routes.append do
    mount ::AhwaaTopicTypes::Engine, at: ::AhwaaTopicTypes::PLUGIN_NAME
  end

  Topic.register_custom_field_type('ahwaa_topic_types', :string)

  # Add to the serializer
  add_to_serializer(:topic_list_item, :ahwaa_topic_type) { object.custom_fields['ahwaa_topic_type'] || 'story' }
  add_to_serializer(:topic_view, :ahwaa_topic_type) { object.topic.custom_fields['ahwaa_topic_type'] || 'story' }
end
