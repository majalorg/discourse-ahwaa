# discourse-ahwaa-topics

Plugin that replace the default layout of the topic list in order to deeply customize it.

> Discourse will call topics to all the posted articles within the forum, everytime a user will "create a topic" it esentially post an article where the community can create "posts" that acts a replies to those topics. In that sense, the forum has many topics and each topic has many posts.

![topic_list](./doc/topic_list.png)

# Contributors

[Alexis Duran](https://github.com/duranmla)
