import { registerUnbound } from "discourse-common/lib/helpers";

// Overriding helper to accept a 'title' argument
registerUnbound("topic-link", (topic, args) => {
  let title = topic.get("fancyTitle");
  if (args.title) {
    title = I18n.t(args.title);
  }

  const url = topic.linked_post_number
    ? topic.urlForPostNumber(topic.linked_post_number)
    : topic.get("lastUnreadUrl");

  const classes = ["title"];
  if (args.class) {
    args.class.split(" ").forEach(c => classes.push(c));
  }

  const result = `<a href='${url}' class='${classes.join(" ")}'>${title}</a>`;
  return new Handlebars.SafeString(result);
});
