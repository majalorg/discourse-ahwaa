import { withPluginApi } from "discourse/lib/plugin-api";

export default {
  name: "ahwaa-topic-init",
  initialize(container) {
    withPluginApi("0.8.9", api => {
      api.modifyClass("model:topic", {
        topicTypeI18n: Ember.computed("ahwaaTopicType", function() {
          return `ahwaa.${this.get("ahwaa_topic_type")}`;
        })
      });
    });
  }
};
