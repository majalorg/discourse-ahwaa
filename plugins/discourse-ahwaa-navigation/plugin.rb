# name: discourse-ahwaa-navigation
# about: A Discourse plugin that add style for components related to navigation
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-navigation

register_asset 'stylesheets/main.scss'
register_asset 'stylesheets/mobile/main.scss', :mobile
