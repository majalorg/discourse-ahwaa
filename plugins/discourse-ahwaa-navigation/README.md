# discourse-ahwaa-navigation

This plugin is in charge of handling styles and customization for all components related to the navigation of the platform such as the built-in `navigation-bar`

## Notes

Please make sure to delete the `categories` item from `top menu` admin settings option, that in order to follow the design expectations of the list controls.

# Contributors

[Alexis Duran](https://github.com/duranmla)
