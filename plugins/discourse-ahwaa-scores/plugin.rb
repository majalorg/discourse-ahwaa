# name: discourse-ahwaa-scores
# about: A Discourse to calculate a user's score based on his stats
# version: 0.0.1
# authors: Ahwaa Team
# url: https://gitlab.com/majalorg/discourse-ahwaa-scores

enabled_site_setting :ahwaa_scores_enabled

after_initialize do
  add_to_serializer(:basic_user, :score) do
    # Cache the score for 1 day since doing
    # this for every user is expensive
    cache_key = "DirectoryItem-user-#{user.id}"
    stats = Rails.cache.fetch("#{cache_key}/all", expires_in: 1.day) do
      DirectoryItem.where(user_id: user.id, period_type: 1).first
    end

    return 0 if stats.nil?

    like_received_points = stats.likes_received * SiteSetting.ahwaa_scores_like_received_points
    like_given_points = stats.likes_given * SiteSetting.ahwaa_scores_like_given_points
    topic_points = stats.topic_count * SiteSetting.ahwaa_scores_topic_points
    topic_entered_points = stats.topics_entered * SiteSetting.ahwaa_scores_topic_entered_points
    post_points = stats.post_count * SiteSetting.ahwaa_scores_post_points
    post_read_points = stats.posts_read * SiteSetting.ahwaa_scores_post_read_points
    day_visited_points = stats.days_visited * SiteSetting.ahwaa_scores_day_visited_points

    [
      like_received_points,
      like_given_points,
      topic_points,
      topic_entered_points,
      post_points,
      post_read_points,
      day_visited_points
    ].sum
  end

  BasicUserSerializer.descendants.each(&:reload)
end
