# discourse-ahwaa-scores

This plugin will provide a `score` key in the `BasicUserSerializer`. The
score is calculated from the stats of the user, store in the
`DirectoryItem` model.

You can customize how much each stat award in the admin settings panel
