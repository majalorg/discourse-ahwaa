# discourse-ahwaa-hero

Plugin that renders the hero component for the ahwaa platform.

![hero](./doc/hero.png)

![hero_2](./doc/hero_2.png)

# Contributors

[Alexis Duran](https://github.com/duranmla)
