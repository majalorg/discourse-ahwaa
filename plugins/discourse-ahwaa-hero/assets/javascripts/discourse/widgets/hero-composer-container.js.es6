import DiscourseURL from 'discourse/lib/url';
import { createWidget } from 'discourse/widgets/widget';
import { h } from 'virtual-dom';

createWidget('hero-composer-container', {
  html(attrs) {
    return h('div.hero.hero--home', [
      h('div.container-fluid.full-width', [
        h('div.row.align-items-end', [
          h('div.col.col-12.col-lg-8', [
            h('h1.hero__title', I18n.t('ahwaa_hero.home.title')),
            h('form.new-post-form', [
              this.attach('hero-composer'),
              this.attach('hero-composer-footer')
            ])
          ]),
          h('div.col.col-12.col-lg-4', [
            this.attach('hero-metadata')
          ])
        ])
      ])
    ]);
  }
});
