import { createWidget } from 'discourse/widgets/widget';
import hbs from 'discourse/widgets/hbs-compiler';

createWidget('hero-composer-footer', {
  tagName: 'footer.new-post-form__footer',

  template: hbs`
  <fieldset id="hero-composer-footer-fieldset" class="new-post-form__type">
    <legend>{{i18n 'ahwaa_hero.composer_label'}}</legend>
    <div class="field-wrap">
      <input type="radio" name="category" id="story" value="story" checked="checked">
      <label for="story">
        <span class="text">{{i18n 'ahwaa_hero.composer_category_story'}}</span>
        <span class="icon icon-icn_09story"></span>
      </label>
    </div>
    <div class="field-wrap">
      <input type="radio" name="category" id="question" value="question">
      <label for="question">
        <span class="text">{{i18n 'ahwaa_hero.composer_category_question'}}</span>
        <span class="icon icon-icn_10question"></span>
      </label>
    </div>
  </fieldset>
  <div class="check-wrap check-wrap--custom">
    <input type="checkbox" name="tags" id="anonymous" value="anonymous">
    <label for="anonymous">
      <span>{{i18n 'ahwaa_hero.composer_tag_anonymous'}}</span>
    </label>
  </div>
  `,

  click(e) {
    var $fieldWrap = $(e.target).parents('.field-wrap');

    if (!$fieldWrap.get(0)) { return; }

    // FIXME: We shouldn't need to add code to handle the radio buttons behavior within a widget
    // NOTE: there is no use of defaultState() method as it generates an error
    this._toggleReadioBtnAttr($fieldWrap.index());
  },

  _toggleReadioBtnAttr(index) {
    var $categoryRadioButtons = $('#hero-composer-footer-fieldset').find('input[type="radio"]');

    // trigger "change" will allow to update the other radio buttons as expected
    $($categoryRadioButtons.get(index - 1)).prop('checked', true).change();
  }
});
