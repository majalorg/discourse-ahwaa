import { CREATE_TOPIC } from 'discourse/models/composer';
import DiscourseURL from 'discourse/lib/url';
import { createWidget } from 'discourse/widgets/widget';
import { h } from 'virtual-dom';

createWidget('hero-composer', {
  tagName: 'div.hero-composer',

  html(attrs) {
    return h('div.hero-composer__edit', [
      h('textarea#new-post', { name: 'body', placeholder: I18n.t('ahwaa_hero.post_new') }),
      h('button.btn.btn-primary.disabled', { type: 'submit' }, [
        h('span', I18n.t('ahwaa_hero.post_submit'))
      ])
    ]);
  },

  click(e) {
    // event source
    var $el = $(e.target);
    var $form = $el.parents('form');

    $form.find('button[type="submit"]').removeClass('disabled');
    $form.addClass('is-focused');

    this._bindFormEvent($form);
  },

  _bindFormEvent($form) {
    if ($.data($form.get(0), 'events')) { return; }

    $form.data({ events: ['openComposer'] });
    $form.on('submit', (e) => {
      e.preventDefault();

      this._openComposerWithParams($form);
    });
  },

  _openComposerWithParams($form) {
    var data = this._formToJSON($form);
    var category = this.site.get('categories').findBy('nameLower', data.category) || {};

    // REVIEW: https://meta.discourse.org/t/open-composer-with-prefilled-information/82915/3
    var composer = Discourse.__container__.lookup('controller:composer');
    composer.open({
      action: CREATE_TOPIC,
      draftKey: 'new_topic',
      title: '',
      topicBody: data.body,
      topicCategoryId: category.id,
      topicTags: data.tags
    });
  },

  _formToJSON($form) {
    var data = {}
    $form.serializeArray().forEach((entry) => {
      data[entry.name] = entry.value;
    });
    return data;
  }
});
