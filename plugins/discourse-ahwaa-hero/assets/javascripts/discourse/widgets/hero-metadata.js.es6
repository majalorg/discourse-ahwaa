import DiscourseURL from 'discourse/lib/url';
import { createWidget } from 'discourse/widgets/widget';
import { h } from 'virtual-dom';

createWidget('hero-metadata', {
  buildKey: () => 'hero-metadata',
  tagName: 'div',

  defaultState() {
    return {
      loaded: false,
      topicsCount: 0,
      postsCount: 0,
      membersCount: 0
    };
  },

  html(attrs, state) {
    if (!state.loaded) {
      this._getMetadata();
    }

    return h('ul.stats', [
      h('li.stats__item.stats__item--posts.icon', [
        h('em', `${state.membersCount}`),
        h('span', I18n.t('ahwaa_hero.members'))
      ]),
      h('li.stats__item.stats__item--topics.icon', [
        h('em', `${state.topicsCount}`),
        h('span', I18n.t('ahwaa_hero.topics'))
      ]),
      h('li.stats__item.stats__item--advice.icon', [
        h('em', `${state.postsCount}`),
        h('span', I18n.t('ahwaa_hero.advice_pieces'))
      ])
    ]);
  },

  _getMetadata() {
    if (this.state.loaded) {
      return;
    }

    var { categories } = this.site;
    var topicsCount = 0;
    var postsCount = 0;

    categories.forEach(c => {
      topicsCount = topicsCount + c.topic_count;
      postsCount = postsCount + c.post_count;
    });

    let params = {
      period: 'weekly',
      order: 'likes_received',
      asc: null,
      name: ''
    };

    this.state.topicsCount = topicsCount;
    this.state.postsCount = postsCount;

    // For the total of users we need to query the store
    this.store.find('directoryItem', params).then(result => {
      this.state.membersCount = result.totalRows;
      this.state.loaded = true;
      this.scheduleRerender();
    });
  }
});
