# name: discourse-ahwaa-hero
# about: A Discourse plugin that renders a hero section
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-hero

register_asset 'stylesheets/main.scss'
