# name: discourse-ahwaa-avatar
# about: A Discourse plugin that adds avatar customization
# version: 1.0.0
# authors: Alexis Duran, Orlando Del Aguila
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-avatar

register_asset 'stylesheets/main.scss'

# This is required when using add_to_serializer, and needs to be true
enabled_site_setting :ahwaa_avatar_trust_level_enabled

after_initialize do
  module AhwaaAvatarLookupExtensions
    def lookup_columns
      @lookup_columns ||= %i[id username uploaded_avatar_id trust_level created_at]
    end
  end

  class ::AvatarLookup
    singleton_class.prepend AhwaaAvatarLookupExtensions
  end

  add_to_serializer(:basic_user, :trust_level) { user.respond_to?(:trust_level) ? user.trust_level : 0 }
  add_to_serializer(:basic_user, :created_at) { user.respond_to?(:created_at) ? user.created_at : DateTime.now }

  # Reload all child serializers, so they include :trust_level and :created_at
  # https://meta.discourse.org/t/inconsistent-behaviour-with-plugins-between-development-and-production/87220
  BasicUserSerializer.descendants.each(&:reload)
end
