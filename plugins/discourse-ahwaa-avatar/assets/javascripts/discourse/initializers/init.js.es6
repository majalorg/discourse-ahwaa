import { on } from "ember-addons/ember-computed-decorators";
import { withPluginApi } from "discourse/lib/plugin-api";
import AvatarBuilder from "discourse/plugins/discourse-ahwaa-avatar/discourse/helpers/avatar";
import RawHtml from "discourse/widgets/raw-html";

export default {
  name: "ahwaa-avatar-init",
  initialize(container) {
    let avatarBuilder = new AvatarBuilder();

    withPluginApi("0.8.9", api => {
      api.decorateWidget("post-avatar:after", helper => {
        let data = helper.getModel();
        return helper.rawHtml(avatarBuilder.html(data, { imageHtml: false }));
      });

      api.decorateWidget("topic-participant:after", helper => {
        let data = helper.widget.attrs;
        return helper.rawHtml(avatarBuilder.html(data, { imageHtml: false }));
      });
    });
  }
};
