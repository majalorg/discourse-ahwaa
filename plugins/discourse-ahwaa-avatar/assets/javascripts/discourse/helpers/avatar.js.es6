import { registerUnbound } from "discourse-common/lib/helpers";
import { avatarImg, formatUsername } from "discourse/lib/utilities";

/**
 * A way to wrap the methods and logic needed in order to render the avatars
 * as expected in all context needed by the platform.
 */
class AvatarBuilder {
  constructor() {
    // usernames that will be excluded to add the trust level customization
    this.excludedUsernames = ["system", "discobot", "anonymous"];
  }

  _template(imageHtml, user) {
    // we will use this invalid level in order to subtle highlight any component without trustLevel data
    let unexpected = 8;
    let tl;

    if (_.include(this.excludedUsernames, user.username)) {
      return imageHtml;
    }

    tl = user.trust_level === undefined ? unexpected : user.trust_level;

    return `
      <div class="custom-avatar user-trust-level-${tl}">
        ${imageHtml}
        <i class="custom-icon fa fa-heart"></i>
      </div>
    `;
  }

  // Basically take this logic from the discourse core as they do not export the method from the user-avatar module
  _discourseRenderAvatar(user, options = {}) {
    if (user) {
      const username = Em.get(user, options.usernamePath || "username");
      const avatarTemplate = Em.get(
        user,
        options.avatarTemplatePath || "avatar_template"
      );

      if (!username || !avatarTemplate) {
        return "";
      }

      let displayName = Ember.get(user, "name") || formatUsername(username);

      let title = options.title;
      if (!title && !options.ignoreTitle) {
        title = Em.get(user, "title");
        if (!title) {
          const description = Em.get(user, "description");
          if (description && description.length > 0) {
            title = displayName + " - " + description;
          }
        }
      }

      // in some cases, user comes with a use attributes nested
      // that has the information we need
      // we do this check in here to make sure we have this information
      user = user.user || user;

      let imageHtml = options.imageHtml;

      // In this case, we don't want to render an image
      // just the trust level (heart)
      if (imageHtml === false) {
        return this._template("", user);
      }

      imageHtml =
        imageHtml ||
        avatarImg({
          size: options.imageSize,
          extraClasses: Em.get(user, "extras") || options.extraClasses,
          title: title || displayName,
          avatarTemplate: avatarTemplate
        });

      return this._template(imageHtml, user);
    } else {
      return "";
    }
  }

  html(user, options) {
    return this._discourseRenderAvatar(user, options);
  }
}

// we need to replace the basic avatar helper implementation to render our avatar with customization
registerUnbound("avatar", function(user, params) {
  let avatarBuilder = new AvatarBuilder();
  return new Handlebars.SafeString(avatarBuilder.html(user, params));
});

export default AvatarBuilder;
