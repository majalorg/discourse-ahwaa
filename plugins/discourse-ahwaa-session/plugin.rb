# name: discourse-ahwaa-session
# about: A Discourse plugin that adds Ahwaa forms to handle user sessions
# version: 1.0.0
# authors: Alexis Duran
# url: https://gitlab.com/majalorg/discourse-ahwaa/plugins/discourse-ahwaa-session

register_asset 'stylesheets/main.scss'
