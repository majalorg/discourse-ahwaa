# discourse-ahwaa-session

Plugin that handles the modal template for LOG-IN and SIGN-UP among whatever view needed for the user to open or close a session within our discourse platform. This plugin mainly aims to overwrite existing discourse templates. Make sure you have `discourse-ahwaa` activated in order this plugin works as expected.

# Contributors

[Alexis Duran](https://github.com/duranmla)
