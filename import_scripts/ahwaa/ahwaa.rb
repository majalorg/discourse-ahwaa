require 'mysql2'
require_relative '../base.rb'

class ImportScripts::Ahwaa < ImportScripts::Base
  ATTACHMENTS_BASE_DIR = __dir__

  EMOJIS = {
    'angel' => ':angel:',
    'colonthree' => ':star_struck:',
    'confused' => ':confused:',
    'cry' => ':cry:',
    'devil' => ':smiling_imp:',
    'frown' => ':frowning_face:',
    'gasp' => ':open_mouth:',
    'glasses' => ':sunglasses:',
    'grin' => ':grin:',
    'grumpy' => ':angry:',
    'heart' => ':heart:',
    'kiki' => ':grimacing:',
    'kiss' => ':kissing_heart:',
    'pacman' => ':stuck_out_tongue_winking_eye:',
    'smile' => ':smile:',
    'squint' => ':expressionless:',
    'sunglasses' => ':sunglasses:',
    'tongue' => ':stuck_out_tongue:',
    'unsure' => ':confused:',
    'upset' => ':angry:',
    'wink' => ':wink:'
  }.freeze

  def initialize
    super

    @client = Mysql2::Client.new(
      host: 'localhost',
      username: 'root',
      password: '',
      database: 'ahwaa_production'
    )

    @anonymous_user_id = nil
  end

  def create_anonymous_user
    user = User.find_by_username('anonymous')

    if user
      @anonymous_user_id = user.id
      return
    end

    password = rand(36**15).to_s(36)
    puts '', "Creating anonymous user, password: #{password}"

    user = User.create!(username: 'anonymous', email: 'noreply@ahwaa.org', password: password)
    user.approve(Discourse.system_user, false)
    user.activate

    @anonymous_user_id = user.id
  end

  def migrate_users
    puts '', 'Migrating Users'

    total_count = @client.query('SELECT COUNT(*) from users').to_a.first['COUNT(*)']
    progress_count = 0
    start_time = Time.now

    create_users(@client.query('SELECT users.*, score_boards.current_points, user_profiles.language FROM users LEFT JOIN score_boards ON score_boards.user_id = users.id LEFT JOIN user_profiles ON user_profiles.user_id = users.id')) do |row|
      progress_count += 1
      print_status(progress_count, total_count, start_time)

      # we don't want to create deleted users
      next if row['deleted'] == 1

      {
        id: row['id'],
        name: row['username'],
        username: row['username'],
        email: row['email'],
        admin: row['is_admin'] == 1,
        created_at: row['created_at'],
        updated_at: row['updated_at'],
        locale: row['language'],
        trust_level: user_trust_level(row['current_points']),
        custom_fields: {
          import_pass: "#{row['encrypted_password']}:#{row['password_salt']}",
          ahwaa_points: row['current_points']
        }
      }
    end
  end

  def migrate_categories
    puts '', 'Migrating Categories'

    total_count = @client.query('SELECT COUNT(*) from tags').to_a.first['COUNT(*)']
    progress_count = 0
    start_time = Time.now

    create_categories(@client.query('SELECT * FROM tags;')) do |row|
      progress_count += 1
      print_status(progress_count, total_count, start_time)

      {
        id: row['id'],
        name: row['name']
      }
    end
  end

  def migrate_topics
    puts '', 'Migrating Topics'

    total_count = @client.query('SELECT COUNT(*) from topics;').to_a.first['COUNT(*)']
    progress_count = 0
    start_time = Time.now

    query = "SELECT topics.*, taggings.tag_id FROM topics LEFT JOIN taggings ON taggings.taggable_id = topics.id AND taggings.taggable_type = 'Topic'"
    create_posts(@client.query(query)) do |row|
      progress_count += 1
      print_status(progress_count, total_count, start_time)

      # if user is missing, is because it was deleted
      # we use the anonymous user in that case
      user_id = user_id_from_imported_user_id(row['user_id']) || @anonymous_user_id

      {
        id: "t#{row['id']}",
        title: replace_smileys(row['title']),
        category: category_id_from_imported_category_id(row['tag_id']),
        raw: replace_smileys(row['content']),
        user_id: user_id,
        created_at: row['created_at'],
        updated_at: row['updated_at']
      }
    end
  end

  def migrate_replies
    puts '', 'Migrating Replies'

    total_count = @client.query('SELECT COUNT(*) from replies;').to_a.first['COUNT(*)']
    progress_count = 0
    start_time = Time.now

    create_posts(@client.query('SELECT * from replies;')) do |row|
      topic_id = topic_lookup_from_imported_post_id("t#{row['topic_id']}")[:topic_id]

      # if user is missing, is because it was deleted
      # we use the anonymous user in that case
      user_id = user_id_from_imported_user_id(row['user_id']) || @anonymous_user_id

      progress_count += 1
      print_status(progress_count, total_count, start_time)

      unless topic_id
        puts "topic #{row['topic_id']} is missing!"
        next
      end

      data = {
        id: "r#{row['id']}",
        raw: replace_smileys(row['content']),
        topic_id: topic_id,
        user_id: user_id,
        created_at: row['created_at'],
        updated_at: row['updated_at']
      }

      if row['parent_id']
        parent = topic_lookup_from_imported_post_id("r#{row['parent_id']}")
        data[:reply_to_post_number] = parent[:post_number] if parent
      end

      data
    end
  end

  # In Ahwaa, only replies have votes
  def migrate_reply_votes
    puts '', 'Migrating Reply likes'

    total_count = @client.query('SELECT COUNT(*) from ratings;').to_a.first['COUNT(*)']
    progress_count = 0
    start_time = Time.now

    @client.query('SELECT * from ratings;').each do |row|
      user = User.new
      post = Post.new

      # we use the anonymous user to preserve likes
      user.id = user_id_from_imported_user_id(row['user_id']) || @anonymous_user_id
      post.id = post_id_from_imported_post_id("r#{row['reply_id']}")

      progress_count += 1
      print_status(progress_count, total_count, start_time)

      next unless user.id && post.id

      begin
        PostAction.act(user, post, PostActionType.types[:like])
      rescue PostAction::AlreadyActed
      end
    end
  end

  # Avatar migration expects avatars files to be in the same directory as this script
  # If this is not the case, modify ATTACHMENTS_BASE_DIR
  def migrate_avatars
    puts '', 'Migrating Avatars'

    user_avatar_map = {}

    @client.query('SELECT users.*, user_profiles.avatar_id FROM users LEFT JOIN user_profiles ON users.id = user_profiles.user_id WHERE users.deleted = 0').each do |row|
      user_avatar_map[row['id']] = row['avatar_id']
    end

    total_count = User.where('id > 0').count
    progress_count = 0
    start_time = Time.now

    User.where('id > 0').find_each do |user|
      progress_count += 1
      print_status(progress_count, total_count, start_time)

      next if !user.custom_fields['import_id'] || user.custom_fields['import_avatar']

      avatar_id = user_avatar_map[user.custom_fields['import_id'].to_i]
      avatar_full_path = "#{ATTACHMENTS_BASE_DIR}#{avatar_map[avatar_id]}"
      avatar_filename = avatar_full_path.split('/').last

      upload = create_upload(user.id, avatar_full_path, avatar_filename)

      if upload.persisted?
        user.import_mode = false
        user.create_user_avatar
        user.import_mode = true
        user.user_avatar.update(custom_upload_id: upload.id)
        user.uploaded_avatar_id = upload.id
        user.custom_fields['import_avatar'] = true
        user.save
      else
        puts "Error: Upload did not persist for #{user.username} #{avatar_filename}!"
      end
    end
  end

  def migrate_private_messages
    puts '', 'Migrating Private messages'

    total_count = @client.query('SELECT COUNT(*) from private_messages;').to_a.first['COUNT(*)']
    progress_count = 0
    start_time = Time.now

    create_posts(@client.query('SELECT * FROM private_messages;')) do |row|
      sender = find_user_by_import_id(row['sender_id'])
      recipient = find_user_by_import_id(row['recipient_id'])

      # skip if sender or recipient are missing
      # in this case we don't care about private messages to deleted users
      if !sender || !recipient
        progress_count += 1
        print_status(progress_count, total_count, start_time)

        next
      end

      data = {
        archetype: Archetype.private_message,
        id: "pm#{row['id']}",
        title: recipient.username.to_s,
        raw: replace_smileys(row['content']),
        user_id: sender.id,
        target_usernames: [recipient.username],
        created_at: row['created_at'],
        updated_at: row['updated_at']
      }

      if row['parent_id']
        parent = topic_lookup_from_imported_post_id("pm#{row['parent_id']}")
        data[:topic_id] = parent[:topic_id] if parent
      end

      progress_count += 1
      print_status(progress_count, total_count, start_time)

      data
    end
  end

  def approve_and_activate_users
    puts '', 'Activating users'

    total_count = User.where('id > 0').count
    progress_count = 0
    start_time = Time.now

    User.where('id > 0').where(approved: false).find_each do |user|
      user.activate
      user.approve(Discourse.system_user, false)
      progress_count += 1
      print_status(progress_count, total_count, start_time)
    end
  end

  def avatar_map
    return @avatar_map if @avatar_map

    @avatar_map = {}

    @client.query('SELECT * from avatars;').each do |row|
      @avatar_map[row['id']] = row['url']
    end

    @avatar_map
  end

  def replace_smileys(content)
    content.gsub(/<img src='\/images\/smileys\/(\w+).gif' border='0' \/>/) { EMOJIS[Regexp.last_match[1]] }
  end

  def user_trust_level(points)
    if points >= 500
      3
    elsif points >= 100
      2
    elsif points >= 15
      1
    else
      0
    end
  end

  def execute
    create_anonymous_user
    migrate_users
    migrate_categories
    migrate_topics
    migrate_replies
    migrate_reply_votes
    migrate_private_messages
    migrate_avatars
    approve_and_activate_users
  end
end

ImportScripts::Ahwaa.new.perform if $PROGRAM_NAME == __FILE__
