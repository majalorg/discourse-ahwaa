Here you will describe the feature you want to get implemented with as much detail as possible. This description should include at least:

* Description of what needs to be implemented.
* How the designs will look (with links or attachments).
